
FROM openjdk:17-jdk-alpine

ENV DB_USER=db_user \
    DB_PWD=db_pwd \
    DB_NAME=db_name \
    DB_SERVER=mysql

EXPOSE 8080

RUN mkdir -p /home/node-app

COPY . /home/node-app

WORKDIR /home/node-app

RUN /bin/sh -c "./gradlew build"

WORKDIR /home/node-app/build/libs

CMD ["java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]
